import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import javafx.scene.shape.Rectangle;


/**
 * Proovin, kas äkki õnnestub JavaFX'i Rectanglet nii palju täiendada,
 * et sel oleks koordinaadid juba küljes. ja mousehandlerid
 * pihtas-möödas saab ka siin joonistada, aga laeva joonistamist ei kujuta praegu kuidagi ette.
 *
 * GraphicalUICell on Rectangle, millel on paar lisaomadust: aadress, ehk X ja Y.
 * suhtestumine hiirekursoriga - muuda värvi ja anna tagasidet kliki kohta.
 * TODO: changeTo(hitorsmthg) - redraw.
 *
 */

public class GraphicalUICell extends StackPane {
//stackPane -> Rectangle for fill color
//          -> Pane for background image - or anything else


    //private Coord aadress;
    public int X; //so 1..10
    public int Y; //ehk koht ruudustikus


    private BattleBoard.Cell cellType = BattleBoard.Cell.WATER; //sellest sõltub, kuidas cell'i joonistada tuleb
    private Rectangle rect = new Rectangle();
    private Pane overlay = new Pane();


    //Üle keskmise toimekas konstruktor
    public GraphicalUICell(int x, int y) {

        this.X = x;
        this.Y = y;
        //this.setPrefSize(35, 35);

        rect.setStroke(Color.ORANGE);
        rect.setFill(Color.STEELBLUE);

        //ImageView img = new ImageView(new Image("resources/ttt_x.png"));
        //overlay.getChildren().add(img);
        overlay.setMouseTransparent(true); //siis jõuab hiireklikk otse Rectangle külge.

        this.getChildren().addAll(rect, overlay);
        rect.widthProperty().bind(this.prefWidthProperty());
        rect.heightProperty().bind(this.prefHeightProperty());


        //Mängija hiireklikk P1 boardil peaks selle ruudu aadressi edastama
        //ja ühtlasi käivitama AI käigu.
        this.rect.setOnMouseClicked(event -> {
            if (GraphicalUICell.this.isInteractive()) {

                try {
                    CellInfo result = GraphicalUI.getP1().board.shoot(GraphicalUICell.this.X, GraphicalUICell.this.Y); //_TODO: see tuleks AIle anda, mitte P1le. P1 peaks olema vaid "üleskirjutuseks"
                    GraphicalUI.getAi().respondToMove(result); //gets new thread.
                    //((GraphicalUIBoard) GraphicalUICell.this.getParent().getParent().getParent()).updateBoard(); //yeah;)
                    GraphicalUI.getP1().updateBoard(); //okei, võib ka nii.


                    //ja anname järje AIle.
                    GraphicalUI.setNextPlayer();
                    GraphicalUI.getAi().takeAShot(); //see mh ootab kuni eelmine comment valmis saab.

                } catch (BattleBoard.AlreadyShotException e) {
                    System.out.println("You already shot here. No big deal.");
                }

            }
        });

        //this.rect.setOnKeyPressed(event -> {
        //    System.out.println(event.getCharacter());
        //});

        //Ainult iluks ja hiire asukoha indikeerimiseks.
        this.rect.setOnMouseEntered(event -> {
            if (! GraphicalUICell.this.isPlayersBoard()) {
                GraphicalUICell.this.rect.setFill(Color.DARKBLUE);

            }
        });
        this.rect.setOnMouseExited(event -> {
            if (! GraphicalUICell.this.isPlayersBoard()) {
                GraphicalUICell.this.rect.setFill(Color.STEELBLUE);
            }
        });

    }
    
   

    //otsustab, kas saab klikkida
    private boolean isInteractive(){
        //System.out.println(GraphicalUI.isPlayersTurn());
        return !this.isPlayersBoard()  && GraphicalUI.isPlayersTurn();
    }

    private boolean isPlayersBoard(){
        return ((GraphicalUIBoard) this.getParent().getParent().getParent()).isPlayerBoard();
        //see inkantatsioon on asendamaks player parameetrit, mis muidu konstruktorit obfuskeeriks. Yeah, kell on palju.
        //see häda tal muidugi on, et loomise hetkel pole veel teada, kuhu cell kuuluma hakkab.
    }


    //muuda cell'i väljanägemist
    public void changeType(BattleBoard.Cell type) {
        this.cellType = type;
        switch (type) {
            case WATER : this.rect.setFill(Color.STEELBLUE); break;

            case SHIP  : if (isPlayersBoard())
                this.rect.setFill(Color.DARKGRAY); break;

            case MISS  : overlay.setStyle("-fx-background-image: url('file:resources/o.png');" +
                             "-fx-background-repeat: no-repeat; -fx-background-size: contain;");
                         break;

            case HIT   : overlay.setStyle("-fx-background-image: url('file:resources/x.png');" +
                             "-fx-background-repeat: no-repeat; -fx-background-size: contain;");
                         if (isPlayersBoard())
                                this.rect.setFill(Color.DARKGRAY);
                         break;

            }

    }





    public BattleBoard.Cell getCellType() {
        return cellType;
    }
    public Rectangle getRect () {return this.rect;}

}
