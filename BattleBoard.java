import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by halo on 5.03.15.
 * BattleBoard tegeleb ühe mänguväljaga. Hoiab seisu, ütleb, mis lasu toimel juhtus jne.
 *
 * Nojah, nüüd kui midagi toimima hakkab, selgub, et gridil on see jant, et ei saa ju aru, kus individuaalne laev algab ja teine lõpeb.
 * Tegelikku gridi on vaja vaid väljajoonistamise hetkel.
 * Seda enam, et igas meetodis kahekordsed tsüklid on lihtsalt koledad.
 *  Palju parem oleks klass Ship, millel oleks vaid alguspunkt ja suund, ning meetod isHit, mida siis kuritarvitatakse mh ka asetamise aegu.
 */

public class BattleBoard implements Serializable {

    //enum? Üks mänguväli võib olla tühi, möödalask, laev, pihtas.
    public enum Cell {
        WATER, MISS, SHIP, HIT
    }
    
    public class  AlreadyShotException extends RuntimeException { }; //juhuks kui teist korda samale ruudule klikata


    private Cell[][] board;
    private byte boardWidth = 10;
    private byte boardHeight = 10;

    private byte[] ships = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};


    Random rand = new Random();

    /****************************************************
     * public meetodid
     ****************************************************/

    //vaikekonstruktor teeb 10*10 juhusliku paigutusega väljaku
    BattleBoard () {
        placeShipsRandomly();
    }

    public CellInfo shoot(int x, int y){

        switch (this.board[x][y]) {
            case SHIP: this.board[x][y] = Cell.HIT; break;
            case WATER: this.board[x][y] = Cell.MISS; break;
            default: throw new AlreadyShotException();
        }

        return new CellInfo(x, y, this.board[x][y]);
    }


    public CellInfo shootRandom() {

        //kui siia on juba lastud, proovi uuesti.. Mnjah.
        while (true) {
            byte xx = (byte) rand.nextInt(boardWidth);
            byte yy = (byte) rand.nextInt(boardHeight);

            try {
                return shoot(xx, yy);
            } catch (AlreadyShotException e) {//läheb lihtsalt uuele ringile
            }
        }
    }

    //Kas leidub mõni laevaosa, mis pole veel põhja lastud?
    public boolean allSunken() {
        for (int i = 0; i < boardWidth; i++)
            for(int j = 0; j < boardHeight; j++)
                if (this.board[i][j] == Cell.SHIP) {
                    return false;
                }

        return true;
    }

    //debugimiseks oli abiks.
    public int countShip() {
        int count = 0;
        for (int i = 0; i < boardWidth; i++)
            for(int j = 0; j < boardHeight; j++)
                if (this.board[i][j] == Cell.SHIP) {
                    count += 1;
                }

        return count;
    }


    //getterid
    public Cell[][] getBoard() { return this.board; }
    public byte getBoardHeight() {
        return boardHeight;
    }
    public byte getBoardWidth() {
        return boardWidth;
    }
    public Cell[][] getShiplessBoard() {
        Cell[][] shipless = copyOfBoard(this.board);
        for (int i = 0; i < boardWidth; i++)
            for(int j = 0; j < boardHeight; j++)
                if (this.board[i][j] == Cell.SHIP) {
                    this.board[i][j] = Cell.WATER;
                }

        return shipless;
    }


    //annab ühe mänguväljaku sõnena. Kui kasutajaVaade, siis näitab ka laevu.
    public String toString( boolean kasutajaVaade ) {

        //TODO:norm sümbolid
        String ret = "";
        for (byte y = 0; y < boardWidth; y++) {
            for (byte x = 0; x < boardHeight; x++) {
                switch (this.board[x][y]) {
                    case WATER:  ret += " "; break;
                    case SHIP:   ret += (kasutajaVaade) ? '▒' : " "; break;
                    case HIT:    ret += "X"; break; //"❌" ei ole mul monospaced1??
                    case MISS:   ret += "•"
                    ; break;
                }
            }
            ret += "\n";
        }
        return ret;
    }

    public String toString() { return this.toString(true); }

    //proovi kõik laevad kuidagi ära paigutada. Vastavalt reeglitele. Praegused reeglid ütlevad, et
    //võib need läbisegi igale poole paigutada, peab ainult vaatama, et üksteise peale ei paneks.
    //algoritm selleks on väga lihtne -
    //aseta juhuslikult. Kui ei saa, proovi, ütleme 100 korda veel.
    //kui ikka ei saa, siis hakka nullist otsast peale.
    //ütleme, et 1000 korda võib proovida, siis tuleks teatada, et kohe kuidagi ei saa.

    public void placeShipsRandomly() {
        int katse = 0;
        int ship  = 0;

        while (katse++ < 1000) {

            initBoard();
            for (ship = 0; ship < ships.length; ship++) {

                for (int n = 0; n < 500; n++) {
                    byte x = (byte) rand.nextInt(boardWidth + 1);  //väga võimalik, et 255+ väljakusuuruse juures tekib ebakõlasid.
                    byte y = (byte) rand.nextInt(boardHeight + 1);

                    if (tryToPlaceShip(ships[ship], x, y)) {
                        break; //kui õnnestus, siis hüppab välja ja võtab järgmise laeva.
                    }

                }
                //siia jõuab siis, kui laeva pole õnnestunud paigutada, või on ships edukalt läbi käidud.
                if (ship == ships.length -1) {
                    //System.out.println("sain laevu " + countShip());
                    return;

                }
            }
            //DEBUG
            System.out.print(".");
        }
        System.out.println();
        throw new RuntimeException("Ei õnnestu laevu ära paigutada");
    } //placeShips


    /****************************************************
     * privaatsed meetodid oma tarbeks
     ****************************************************/

    //tühjenda väljakumassiiv.
    private void initBoard(){
        if (this.board == null) {
            this.board = new Cell[boardWidth][boardHeight];
        }

        for (int x = 0; x < boardWidth; x++)
            for(int y = 0; y < boardHeight; y++)
                this.board[x][y] = Cell.WATER;
    }



    //XY kohale proovi laeva paigutada kahes suunas.
    //juba proovimise ajal kirjutab ennast massiivi üle
    //selleks on boardi koopiat vaja;(
    private boolean tryToPlaceShip(byte shiplength, byte x, byte y) {
        Cell[][] backup =
                        //this.board.clone();
                        copyOfBoard( this.board ); //deep copy ikkagi hädavajalik, sest mul on 2D array

        int xx, yy;
        //mis suunast alustan
        boolean direction = rand.nextBoolean();

        for (int i=0; i < shiplength; i++) {
            //System.out.print(i);
            xx = (direction) ? x : x+i;
            yy = (direction) ? y+i : y;


            //väldin üle serva laskmist.
            if (xx >= boardWidth || yy >= boardHeight) {
                this.board = backup;
                return false;
            }

            //muidu proovi paigutada
            if (this.board[xx][yy] == Cell.WATER) {
                this.board[xx][yy] = Cell.SHIP;
            } else {
                this.board = backup;
                return false;
            }
        }
        return true;
    }


    //proovime natuke deep'imat clone()
    private Cell[][] copyOfBoard(Cell[][] original) {
        Cell[][] newBoard = new Cell[original.length][];

        for (int x = 0; x < original.length; x++) {
            newBoard[x] = original[x].clone();
        }
        return newBoard;
    }
    //paha häkk
    public void setBoard(Cell[][] board) {
        this.board = board;
    }

}
