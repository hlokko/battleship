
import javafx.beans.binding.DoubleBinding;
import javafx.geometry.HPos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.text.Text;


/**
 * See peaks joonistama  ühte mänguväljakut.
 */
public class GraphicalUIBoard extends Group {

    public BattleBoard board; //siis käiks kogu tegevus ikka läbi P2.board....


    private static final double GAP = 1;
    private static int cellSize = 35;


    private GridPane grid;
    private int nRows;
    private int nCols;

    private AnchorPane stack = new AnchorPane();

    public boolean isPlayerBoard;



    // Toimekas konstruktor
    public GraphicalUIBoard(int nRows, int nCols, boolean isPlayer) {
        //tilePane.setStyle("-fx-background-color: rgba(255, 215, 0, 0.1);");
        //super();
        super.getChildren().addAll(stack);

        grid = new GridPane();
        stack.getChildren().addAll(grid);


        grid.setHgap(GAP);
        grid.setVgap(GAP);
        
        this.isPlayerBoard = isPlayer;
        this.nCols = nCols;
        this.nRows = nRows;


        this.createCells();


        this.board = new BattleBoard();

        //laevajoonistus
        //this.overlay = new Group();
        //super.getChildren().add(overlay);
        //saab küll, aga porri on rohkem kui raha eest
        //Image hit = new Image("resources/ttt_x.png");
        //ImageView img = new ImageView(hit);
        //img.setX(200);
        //img.setY(30);
        //overlay.getChildren().add(img);
        //super.getChildren().add(img);
        //stack.getChildren().add(img);

    }

    //Mängu laadimisel oleks võibolla hea anda ette hoopis failist võetud board, ja player.
    public GraphicalUIBoard(BattleBoard b, boolean isPlayer) {
        this(b.getBoardWidth(), b.getBoardHeight(), isPlayer); //kutsub välja esimese konstruktori,
        // mis küll teeb jälle ühe random boardi, aga:
        this.board = b;
        //this.updateBoard();
        System.out.println("Boardgen Done");
    }


    //värskendab väljanägemist. Tuimalt joonsitab kõik üle.
    public void updateBoard() {

        for (Node node : this.grid.getChildren()){
            if(node instanceof GraphicalUICell){
                GraphicalUICell cell = (GraphicalUICell) node;
                int x = cell.X;
                int y = cell.Y;
                cell.changeType(board.getBoard()[x][y]);

                //if(this.isPlayerBoard && board.getBoard()[x][y] == BattleBoard.Cell.SHIP)
                //    cell.changeType(BattleBoard.Cell.SHIP);
            }
        }

        if (this.board.allSunken())
            GraphicalUI.gameOver(!this.isPlayerBoard);
    }


    //
    private void createCells() {

        grid.getChildren().clear(); //kustutab senised cell'id ja teeb uued.

        //vasak ülanurk on tühi.
        grid.add(new Text(""), 0, 0);
        grid.getRowConstraints().add(new RowConstraints( cellSize ));   //äärmised read/veerud võiksid mingi laiusega olla
        grid.getColumnConstraints().add(new ColumnConstraints( cellSize ));



        //header A..J
        for (int i = 0; i < nCols; i++) {
            Text text = new Text(Character.toString((char) (65 + i))); // int (65) -> char ('A') -> String ("A")
            grid.setHalignment(text, HPos.CENTER);
            grid.add(text, i + 1, 0);
        }

        //tekitab read
        for (int i = 0; i < nRows; i++) {

            //numbrid 1..10
            Text text = new Text( Integer.toString(i+1));
            grid.setHalignment(text, HPos.CENTER);
            grid.add(text, 0, i + 1);

            //row of cells
            for (int j = 0; j < nCols; j++) {
                grid.add(new GraphicalUICell(i, j), i + 1, j + 1);
            }
 
        }
    }



    //called only once, binding takes care of the rest (hopefully)
    public void resizeToFit(DoubleBinding usableWidth) {
        for (Node cell : this.grid.getChildren()) { //tekst on ka..
            if (cell instanceof GraphicalUICell) {
                //((GraphicalUICell) rect).resetCoordinates();

                ((GraphicalUICell) cell).prefWidthProperty().bind( usableWidth.divide(nCols) );
                ((GraphicalUICell) cell).prefHeightProperty().bind( usableWidth.divide(nCols) );


            }
        }
    }


//    public GridPane getBoard() {
//        return this;
//    }

    public boolean isPlayerBoard() {
        return isPlayerBoard;
    }

}