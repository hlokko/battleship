
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

public class Warning {
	//Hoiatus, mis tagastab booleani vastuse kohta
	
	private static boolean input;
	
    public static boolean warningWindow(String message) {
        Stage window = new Stage();
        
        //loon stage-i
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Hoiatus!");
        window.setWidth(250);
        window.setHeight(100);

        //loon jah nupu
        Label label = new Label();
        label.setText(message);
        Button yesButton = new Button("Jah");
        yesButton.setOnAction(e -> {
        	input = true;
        	window.close();
        });
        yesButton.setMinWidth(50);
        
      //loon ei nupu
        Button noButton = new Button("Ei");
        noButton.setOnAction(e -> {
        	input = false;
        	window.close();
        });
        noButton.setMinWidth(50);

        //Teade tuleb esimesse kasti, nupud teise kasti
        HBox text = new HBox();
        HBox buttons = new HBox(30);
        text.getChildren().addAll(label);
        text.setAlignment(Pos.CENTER);
        buttons.getChildren().addAll(yesButton, noButton);
        buttons.setAlignment(Pos.CENTER);

        VBox vKast = new VBox(10);
        vKast.getChildren().addAll(text, buttons);
        
        Scene scene = new Scene(vKast);
        window.setScene(scene);
        window.setResizable(false);
        window.showAndWait();
        
        return input;
    }

}