import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.*;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.awt.*;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.Semaphore;


//Ideaalne AI simuleeriks täielikult inimest - teaks vaid oma mänguvälja, (ja UI ei teaks),
//mõtleks, ja ütleks vaid oma kommentaari ja koordinaadid, mille kohta see käib.
//See siin päris ideaal veel ei ole, aga peaaegu;)

public class AI {


     //odot. vastus AIlt tulgu propertytega, aga talle ei hakka seda luksust lubama.
     private SimpleStringProperty comment = new SimpleStringProperty(); //kommentaar, mis suunatakse TextAreale. setValue toimub sh teises threadis, st, et listeneril peab olema runLater
     private SimpleObjectProperty<CellInfo> shot = new SimpleObjectProperty<CellInfo>( new CellInfo() ); //Kui see saab newShotInfo, siis teab GUI enda olekut värskendada

     private BattleBoard ownBoard;
     private BattleBoard opponentsBoard; //mis tegelikult peaks algselt tühi olema.


     //http://java2s.com/Tutorials/Java/Thread/How_to_use_Java_Semaphore_to_coordinate_threads.htm
     private Semaphore lock = new Semaphore(1, true);  //üks luba, nõutud järjekorras
     //kui parasjagu tegeleb teksti väljastamisega, siis peaks järgmise tegevusega ootama seni kuni eelmine on valmis.
     //Lihtsalt et sassi ei läheks. Kuigi selle jaoks on ka ExecutorService vmt

    static boolean kasViivitus = true;
    private Random rand = new Random();


    //getterid, mida GUIl vaja lähebb
    public SimpleObjectProperty<CellInfo> getShotInfoProperty() { return this.shot; }
    public SimpleStringProperty getCommentProperty() { return this.comment; }
    public void setOpponentsBoard(BattleBoard opponentsBoard) {this.opponentsBoard = opponentsBoard;}
    public void setOwnBoard(BattleBoard ownBoard) {this.ownBoard = ownBoard;}


    public void init() { // just for laughs.

        LinkedList<String> bootmessages = new LinkedList<String>();
        bootmessages.add("Warming up nuclear reactors...");
        bootmessages.add("Discussing strategies for taking over world...");
        bootmessages.add("Laughing out loud (with maniacal undertone)...");
        bootmessages.add("Loading native language modules...");
        bootmessages.add("Setting up personality disorders...");
        bootmessages.add("Reading game manual...");
        bootmessages.add("Activating orbital defences...");
        bootmessages.add("Sabotaging command lines of Pentagon...");
        bootmessages.add("Initiating super secret project \"Singularity\"...");
        int mitu = bootmessages.size() / 3 + rand.nextInt(2); //võtab mingi valimi, mitte kõike.

        new Thread(() -> {

                comment.setValue("AI booting...\n");
                viivitus(1);

                for (int i=0; i<mitu; i++) {

                    comment.setValue( bootmessages.remove(rand.nextInt(bootmessages.size())) ); //remove, et kaks korda sama asi ei tuleks

                    int r = rand.nextInt(7);
                    for (int j=0; j < r; j++) {
                        viivitus( rand.nextDouble() );
                        comment.setValue(".");
                    }

                    comment.setValue("Done.\n");
                }

                comment.setValue("I am online.\n\n");
                viivitus(0.9);
                comment.setValue("WTF? Battle stations! \n" +
                        "This is not a drill, repeat, this is not a drill!\n\n");

        }).start();

    };



     //see väljastab juttu (tegelikult muudab SimpleStringProperty't) juhul kui kasutaja teeb käigu.
     public void respondToMove(CellInfo opponentsShot) {

         //kui on juba pikem jutt ootamas, siis laseks selle välja padistada. Muidu ei jõua ära kannatada.
         //TODO: Äkitselt see enam ei tööta??? Enne töötas..
         if (lock.getQueueLength() > 0) {
             kasViivitus = false;
             comment.setValue("\n" + "Ootan'd pisut, kus sa tormad..." + "\n");
         } else kasViivitus = true;


         new Thread(() -> {
             try {
                 lock.acquire(); //ootab, kuni lukk vabaneb
                 viivitus( rand.nextDouble() * 2 );
                 String paneTeksti = (opponentsShot.isHit()) ? pHit[rand.nextInt(pHit.length)] : pMiss[rand.nextInt(pMiss.length)];

                 comment.setValue(paneTeksti);

             } catch (InterruptedException e) {
             } finally {
                 lock.release();
                 comment.setValue("\n"); //et update toimuks. Objectpropertyl on olemas fireValueChangedEvent, sellel mitte.
             }
         }).start();

      };


     //shot.setValue( new CellInfo(1, 2, BattleBoard.Cell.HIT));
     public void takeAShot() {

         new Thread(() -> {
             try {
                 lock.acquire();
                 viivitus(rand.nextDouble());

                 shot.setValue( opponentsBoard.shootRandom() ); //põhiline erinevus eelmisest... TODO: Tähendab, saaks ka kokku võtta.
                 String paneTeksti = (shot.getValue().isHit()) ? cHit[rand.nextInt(cHit.length)] : cMiss[rand.nextInt(cMiss.length)];

                 viivitus(rand.nextDouble());
                 comment.setValue(paneTeksti);

             } catch (InterruptedException e) {
             } finally {
                 lock.release();
                 comment.setValue("\n"); //et update toimuks
             }
         }).start();

     }


     public boolean isThinking() {
         return lock.getQueueLength() > 0;
     }


	//Siia panen listidesse erinevad laused, mida arvuti jutustab
    //arvuti lasi mööda
    static String[] cMiss = {"Lasin mööda vaid haletsusest.",
    		"See käik aitab sul natuke järgi jõuda! ...mitte et sul lootust oleks!"		
    };
    //arvuti lasi pihta
    static String[] cHit = {"Õpi, vaat nii mängitakse!"
    };
    //Mängija lasi mööda
    static String[] pMiss = {"Mulle tundub, et sa isegi ei ürita.",
    		"See mäng käib sul vist üle jõu!",
    		"Palun ära raiska mu aega!",
    		"Võib-olla peaksid valima mõne lihtsama mängu?"
    };
    //Mängija lasi pihta
    static String[] pHit = {"Viimaks ometi üks korralik käik!",
    		"Kui selliseid käike oleks rohkem, siis oleks see mäng isegi nauditav."
    };




    //H: Ettepanek2: kui tahta loogika ühte kohta koondada, siis saaks need stringid ka siia tuua, ning sellele
    //meetodile anda kaks parameetrit - mängija (kelle kord), ja kas lask on pihtas või möödas.
    //Btw, kas shoot() peaks tagastama mitte booleani, vaid mingi omatüübi - on ju lasul pigem 4 võimalust: pihtas, põhjas(veeleiole), mööda ja illegaalne lask?

    private static String reaction(String[] laused){
    	Random random = new Random();
    	int lauseid = laused.length;
    	return laused[random.nextInt(lauseid)];
    }


    //Meetod tekitab etteantud aja pikkuse viivituse. Aeg anda sekundites (sobivad ka murdarvud)
    private void viivitus(double aeg){//TODO hiljem lisada randomizer
    	if (kasViivitus){
	    	try{
				Thread.sleep((int)(1000*aeg));
			}
			catch (InterruptedException e){}
    	}
        else System.out.println("Viivituseta");
    }


    /*
     * Task võiks ka hästi sobida, aga Thread oli ennem.
     Task<Integer> task = new Task<Integer>() {
         @Override protected Integer call() throws Exception {
             int iterations = 0;
             for (iterations = 0; iterations < 100000; iterations++) {
                 if (isCancelled()) {
                     break;
                 }
                 System.out.println("Iteration " + iterations);
             }
             return iterations;
         }

         @Override protected void succeeded() {
             super.succeeded();
             updateMessage("Done!");
         }

     *
     * */
         /* or possibly simply: ? Vahest polekski muud vaja. Aga Timeline ma häesti ei mõista ja praegu tudeerida ka ei taha* /
         Timeline jookseb Application threadis, paralleelselt. Oleks igati jonks.
         new Timeline(new KeyFrame( Duration.millis(2500),
                 uh -> { comment.setValue("Timeframeee");
                         comment.setValue("\n");
                          }
         )).play();
         */

    public BattleBoard getOwnBoard() {
        return ownBoard;
    }

 /*
    //Meetod tegeleb mängija käiguga
    public void run(){
    	käik = true;

        boolean kasPihtas = P2.shoot(xkoord, ykoord);
        
        String[] tekst = (kasPihtas) ? pHit : pMiss;

        kuvaMänguväljak();
        viivitus(0.5);
    	System.out.println( reaction(tekst) );
    	
    	Platform.runLater(new Runnable() {
            @Override public void run() {
            	for (Node rect : GraphicalUI.P1.getChildren()) { //tekst on ka..
                    if (rect instanceof GraphicalUICell) {
                        ((GraphicalUICell) rect).setFill(Color.BLACK);
                        break;
                    }
            	}
            }
        });
    	viivitus(1);
    	
    	cTurn();
    	käik = false;
    	//see peaks nüüd võimaldama fx poole pöörduda
    	

    }

/*

    //tegeleb arvuti käiguga
    private static void cTurn(){

    	System.out.println("Nüüd on minu kord rünnata! Hmm...");

    	viivitus(3);
    	
    	System.out.println("Hoia alt!");
    	viivitus(1);
    
    	if(P1.shootRandom()){
    		kuvaMänguväljak();
    		viivitus(0.5);
    		System.out.println(reaction(cHit));
    	}
    	else{
    		kuvaMänguväljak();
    		viivitus(0.5);
    		System.out.println(reaction(cMiss));
    	}
    }



    //näita kõrvuti mängija ja AI väljakut
    //battleboard.toString annab väljakusisu ühe mitmerealise stringina.
    //siin lammutan selle ridahaaval järjendisse, ja panen kõrvuti kokku
    public static void kuvaMänguväljak() {

        Boolean mängijavaade = true;
        String[] board1 = P1.toString( mängijavaade).split("\n");
        String[] board2 = P2.toString(!mängijavaade).split("\n");

        StringBuilder korvuti = new StringBuilder();

        korvuti.append("          1                   1\n");
        korvuti.append(" 1234567890          1234567890\n");
        korvuti.append("┌──────────┐        ┌──────────┐\n");

        for (int i = 0; i < board1.length; i++) {
            korvuti.append("│" + board1[i] + "│   ");
            korvuti.append(String.format("%2d", i+1)); //rea number keskele
            korvuti.append("   │" + board2[i] + "│\n");
        }
        korvuti.append("└──────────┘        └──────────┘\n");


        System.out.println(korvuti);
    }
*/
}