import java.awt.*;

import javafx.scene.control.Button;
import javafx.scene.effect.Glow;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.util.Duration;
import javafx.animation.FadeTransition;
import javafx.geometry.*;

import java.net.*;


//H: leidsin juttu sellest, kuidas mitmele nupule korraga stiili lisada.
//http://stackoverflow.com/questions/29238835/style-multiple-buttons-at-once-javafx





//Siia tuleks luua menüü, kust saab muuta seadeid, salvestada etc.
public class Menu {
	
	private static int nupuLaius = 200;// ja nupuKorgus on nupu suuruse jaoks
    private static int nupuKorgus = 50;
    private static int appearSpeed = 100;//Kiirus, millega nupp menüüse ilmub
    private static int aDelay = 250;//Viivis enne nuppude ilmuma hakkamist
    private static Stage mMenu; //et saak menüüd Saving'st sulgeda


    public static int getNupuLaius() {
		return nupuLaius;
	}

	public static int getNupuKorgus() {
		return nupuKorgus;
	}
	
	public static int getAppearSpeed() {
		return appearSpeed;
	}

	public static int getaDelay() {
		return aDelay;
	}



	public static void mainMenu() {
		
		//lean tausta
        BackgroundImage background= new BackgroundImage(new Image("file:resources/battleship.png", 300, 500, false, false),
    	        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
    	          BackgroundSize.DEFAULT);
      
        mMenu = new Stage(StageStyle.UNDECORATED);

        mMenu.initModality(Modality.APPLICATION_MODAL);//lülitan teised aknad välja
        mMenu.setTitle("Peamenüü");
        mMenu.setWidth(300);
        mMenu.setHeight(500);
        mMenu.setResizable(false);

        //Tekst, mis indikeerib mängu lõpuseisust. Vmt. Tegelikult peaks kavalamalt tegema.
        Text gameoverText = new Text(GraphicalUI.getMenuMessage());
        gameoverText.setFont(Font.font(40));
        gameoverText.setFill(javafx.scene.paint.Color.CYAN);
        gameoverText.setEffect(new Glow(1.0));



        String olek;//olek seatakse tekstiks nupule, mis alustab mänguga
        if (GraphicalUI.isMängKäib()){
        	olek = "TAGASI MÄNGU";
        }
        else{
        	olek = "ALUSTA MÄNGUGA";
        }
        
        //Antud nupp lubab naasta mängu 
        Button backButton = new Button(olek);
        backButton.setOnAction(e -> mMenu.close());
        backButton.setMinWidth(nupuLaius);
        backButton.setMinHeight(nupuKorgus);
        backButton.setOpacity(0);
        FadeTransition fadeIn0 = new FadeTransition(Duration.millis(appearSpeed));//TODO kui fade in  tundub imelik, siis kustutada
        fadeIn0.setFromValue(0);
        fadeIn0.setToValue(0.7);
        fadeIn0.setNode(backButton);
        fadeIn0.setDelay(Duration.millis(aDelay+appearSpeed));
        
        //nuppude kohta saab infot neile seatud tekstist
      //save menu asjad
    	VBox savingLayout = new VBox(10);
        savingLayout.getChildren().addAll(savlod(true, 0), savlod(true, 1), savlod(true, 2), savlod(true, 3), savlod(true, 4));
        savingLayout.setAlignment(Pos.BASELINE_CENTER);
        savingLayout.setBackground(new Background(background));
        Scene savingScene = new Scene(savingLayout);

        //saveButton ESINEB 2 KORDA: 1) SIIN 2) CLASSIS Saving!!!
        //nupp, mis avavab save menu
        Button saveButton = new Button("SALVESTA");

        if (GraphicalUI.isMängKäib()) {
            saveButton.setOnAction(e -> {
                mMenu.setScene(savingScene);
            });
        }
        else {
            saveButton.setDisable(true); //kui mäng veel ei käi, siis olgu disabled
            saveButton.setStyle("-fx-background-color: #c0c0c0;");
        }

        saveButton.setMinWidth(nupuLaius);
        saveButton.setMinHeight(nupuKorgus);
        saveButton.setOpacity(0);
        FadeTransition fadeIn1 = new FadeTransition(Duration.millis(appearSpeed));
        fadeIn1.setFromValue(0);
        fadeIn1.setToValue(0.7);
        fadeIn1.setNode(saveButton);
        fadeIn1.setDelay(Duration.millis(aDelay+2*appearSpeed));//TODO tundub, et ikka peab vist natuke koodi ilusamaks tegema 
        
        
      //load menu asjad
        VBox loadingLayout = new VBox(10);
        loadingLayout.getChildren().addAll(savlod(false, 0), savlod(false, 1), savlod(false, 2), savlod(false, 3), savlod(false, 4));
        loadingLayout.setAlignment(Pos.BASELINE_CENTER);
        
        Scene loadingScene = new Scene(loadingLayout);
        loadingLayout.setBackground(new Background(background));
        
        //
        Button loadButton = new Button("LAE MÄNG");
        loadButton.setOnAction(e -> mMenu.setScene(loadingScene) );
        loadButton.setMinWidth(nupuLaius);
        loadButton.setMinHeight(nupuKorgus);
        loadButton.setOpacity(0);
        FadeTransition fadeIn2 = new FadeTransition(Duration.millis(appearSpeed));
        fadeIn2.setFromValue(0);
        fadeIn2.setToValue(0.7);
        fadeIn2.setNode(loadButton);
        fadeIn2.setDelay(Duration.millis(aDelay+3*appearSpeed));
        
        Button settingsButton = new Button("SEADED");
        settingsButton.setOnAction(e -> {
        	MessageBox.messageWindow("Seadeid saab muuta vaid premium versioonis!");
        });//TODO muuta prindid meetodite väljakutsumise vastu
        settingsButton.setMinWidth(nupuLaius);
        settingsButton.setMinHeight(nupuKorgus);
        settingsButton.setOpacity(0);
        FadeTransition fadeIn3 = new FadeTransition(Duration.millis(appearSpeed));
        fadeIn3.setFromValue(0);
        fadeIn3.setToValue(0.7);
        fadeIn3.setNode(settingsButton);
        fadeIn3.setDelay(Duration.millis(aDelay+4*appearSpeed));
        
        //exitButton sulgeb mängu
        Button exitButton = new Button("LAHKU MÄNGUST");
        exitButton.setOnAction(e -> {
        	if (Warning.warningWindow("Oled kindel, et soovid mängust väljuda?")){
        		System.exit(0);
        	}
        });
        exitButton.setMinWidth(nupuLaius);
        exitButton.setMinHeight(nupuKorgus);
        exitButton.setOpacity(0);
        FadeTransition fadeIn4 = new FadeTransition(Duration.millis(appearSpeed));
        fadeIn4.setFromValue(0);
        fadeIn4.setToValue(0.7);
        fadeIn4.setNode(exitButton);
        fadeIn4.setDelay(Duration.millis(aDelay+5*appearSpeed));
        
        Button helpButton = new Button("ABI");
        helpButton.setOnAction(e -> {
        	MessageBox.messageWindow("Aitan Google'i avada!");
        	try {
				java.awt.Desktop.getDesktop().browse(
						new URL("https://www.google.ee/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=laevade%20pommitamine").toURI());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        });//MessageBox.messageWindow("Abi saab vaid premium versioonis!"));
        helpButton.setMinWidth(nupuLaius);
        helpButton.setMinHeight(nupuKorgus);
        helpButton.setOpacity(0);
        FadeTransition fadeIn5 = new FadeTransition(Duration.millis(appearSpeed));
        fadeIn5.setFromValue(0);
        fadeIn5.setToValue(0.7);
        fadeIn5.setNode(helpButton);
        fadeIn5.setDelay(Duration.millis(aDelay+6*appearSpeed));
        
        
        //main menu asjad
        VBox layout = new VBox(10);
        layout.getChildren().addAll(new Text(), gameoverText, backButton, saveButton, loadButton, settingsButton, exitButton, helpButton);
        layout.setAlignment(Pos.BASELINE_CENTER);

        
        Scene scene = new Scene(layout);
        
      //Lisan menule tausta
    	layout.setBackground(new Background(background));
        
        mMenu.setScene(scene);
        fadeIn0.play();
        fadeIn1.play();
        fadeIn2.play();
        fadeIn3.play();
        fadeIn4.play();
        fadeIn5.play();


        //Hmm, aga saab ikkagi lambdat kasutada.
        //kui teha ta kõigepealt lambdaks, ja alles siis sisu lisada... Ilmselt on sel mingi loogiline selgitus,
        //ja tegu on rohkem Intellij ja Eclipse ühise probleemiga, mitte lambda piiratusega.
        scene.setOnKeyPressed(keyEvent -> {
            if ((keyEvent.getCode() == KeyCode.ESCAPE) && GraphicalUI.isMängKäib()) {
                mMenu.close();
            }
        });


        //Kui vajutada savingScenes esc avatakse main menu
        savingScene.setOnKeyPressed(keyEvent -> {
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    mMenu.setScene(scene);
                }
            });
        loadingScene.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ESCAPE) {
                mMenu.setScene(scene);
            }
        });

        
        mMenu.showAndWait();
        
	    }
	
	//meetod mugavamaks laadimis/salvestamisnuppude lisamiseks
	private static Button savlod(boolean savingOP, int num){
		return Saving.saveButton(savingOP, num);
	}
	
	public static FadeTransition fadeIn(Button button, int num){
		FadeTransition fadeIn = new FadeTransition(Duration.millis(appearSpeed));
		fadeIn.setFromValue(0);
        fadeIn.setToValue(0.7);
        fadeIn.setNode(button);
        fadeIn.setDelay(Duration.millis(aDelay+num*appearSpeed));
        return fadeIn;
		
	}

    public static void closeMenu () { mMenu.close(); }
}
