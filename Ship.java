/**
 * Created by halo on 10.03.15.
 */
public class Ship {
    public enum Direction {N, E}; //kahest suunast piisab küll

    private int x0; //alguspunkt
    private int y0;

    private int length; //pikkus
    private Direction direction; //suund
    private boolean[] hits; //järjend mööda laeva keret


    public Ship(int x, int y, int length, boolean vertical) {
        this.x0 = x;
        this.y0 = y;
        this.length = length;
        this.direction = (vertical) ? Direction.N : Direction.E;

        this.hits = new boolean[length];
    }


    //vt, kui xy jääb laeva sisse, siis tagasta true, ja muuda hits[]'i
    //selleks käingi läbi hits[]
    //TODO: Väga analoogselt tahan teada, kas antud kohale saab teist laeva paigutada, siis ainult ei taha ma hits[]i uuendada.


    public boolean isHit(int shotX, int shotY) {

        //on mõtet vaadata, kui lask on samal teljel
        if (this.x0 == shotX || this.y0 == shotY) {

            int x; //muutuvad koordinaadid, mida vaatan
            int y;

            //siis käin mööda kujuteldavat laevakeret, alustades x0st
            for (int i = 0; i < this.hits.length; i++) {

                x = (this.direction == Direction.N ) ? this.x0 + i : this.x0;
                y = (this.direction == Direction.N ) ? this.y0     : this.y0 + i;

                if (x == shotX && y == shotY) {
                        hits[i] = true;
                        return true;
                }
            }
        }
        return false;
    }

    //analoogselt isShip koordinaatidel, mis midagi ei muuda, annab ainult informatsiooni
    //TODO: kuidagi kokku võtta
    public boolean isShip(int shotX, int shotY) {
        if (this.x0 == shotX || this.y0 == shotY) {

            int x; //muutuvad koordinaadid, mida vaatan
            int y;

            //siis käin mööda kujuteldavat laevakeret, alustades x0st
            for (int i = 0; i < this.hits.length; i++) {

                x = (this.direction == Direction.N ) ? this.x0 + i : this.x0;
                y = (this.direction == Direction.N ) ? this.y0     : this.y0 + i;

                if (x == shotX && y == shotY) {
                //    hits[i] = true;
                    return true;
                }
            }
        }
        return false;
    }

    //kui hits[] järjendis on mõni false, ehk pihtasaamata koht, siis...
    public boolean isSunken() {
        for (boolean b : hits)
            if (!b) return false;
        return true;
    }

}
