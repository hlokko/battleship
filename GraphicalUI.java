import java.io.Serializable;

import javafx.application.Application;
import javafx.beans.binding.DoubleBinding;
import javafx.event.Event;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.Property;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;



// Väga hea sissejuhatav kirjeldus layoutidesse: https://docs.oracle.com/javafx/2/layout/builtin_layouts.htm

public class GraphicalUI extends Application {
	
	private static boolean mängKäib = false;   //menüüs muudab nuputeksti
    private static String menuMessage = "Battleship"; //Kiri, mida menüüs näidata. Ei kuidagi hästi lihtsalt gameover edastada..
    private static boolean playersTurn = true; //mingi enum oleks semantiliselt viisakam. ja setNext.. .
    private static Stage primaryStage; //et oleks kättesaadav

	private static GraphicalUIBoard P1;        //AI mängulaud
    private static GraphicalUIBoard P2;        //inimmängija mängulaud
    private static AI ai;

    //setters
    public static void setNextPlayer() { playersTurn = !playersTurn; }
    public static void setP1(GraphicalUIBoard p1) { P1 = p1; setupGUI(); P1.updateBoard(); ai.setOwnBoard(P1.board);}
    public static void setP2(GraphicalUIBoard p2) { P2 = p2; setupGUI(); P2.updateBoard(); ai.setOpponentsBoard(P2.board);}
    public static void setPlayersTurn(boolean p) { playersTurn = p;}
    public static void setMängKäib(boolean mängKäib) {GraphicalUI.mängKäib = mängKäib;}
    public static void setMenuMessage(String menuMessage) {GraphicalUI.menuMessage = menuMessage;//Selle muutmine on vajalik laadimisel
	}
	//getters
    public static boolean isPlayersTurn() { return playersTurn; }
    public static GraphicalUIBoard getP1() {return P1; }
    public static GraphicalUIBoard getP2() {return P2;}
    public static AI getAi() {return ai;}
    public static boolean isMängKäib() {
        return mängKäib;
    }
    public static String getMenuMessage() {return menuMessage; }

    @Override
    public void start(Stage primarySt) {
        primaryStage = primarySt;

        ai = new AI();        //käivitab pöördumatu protsessi peatseks singularity saabumiseks.
        P1 = new GraphicalUIBoard(10, 10, false); //AI
        P2 = new GraphicalUIBoard(10, 10, true);
        
        //Liigutasin main menu peale mänguväljade loomist, et saaks mängu laadida enne mängu alustamis
    	//Avan main menu
    	Menu.mainMenu();

        setupGUI();  //Kui menu just teisiti ei korralda, siis seab mänguakna valmis
        P2.updateBoard();        //esmane ülesjoonistus (laevade indikeerimiseks). Liiga vara ei tohi välja kutsuda, GUI peab olema valmis tehtud... Doh.

        mängKäib = true;
        TextArea chatbox = (TextArea) primaryStage.getScene().lookup("#chatBox"); //nojah, aga võib ka lihtsalt ühe static variable teha. Aga hea teada, et ka nii saab.

        ai.init();
        ai.setOwnBoard(P1.board);
        ai.setOpponentsBoard(P2.board); //TODO: igale päris oma board, millest teised ei pea midagi teadma. Va. salvestamise juhul.

        //K: omaarust ei muutnud järgnevas commentis midagi kuid sourcetree ütleb teisiti
        //commentProperty on String, mille muutmisel rakendub changeValueListener,
        //mis runLateriga värskendab chatboxi sisu.
        ai.getCommentProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() ->
                    chatbox.appendText(newValue));
        });


        //Kui AI otsustab mingi lasu teha, siis siin püütakse see kinni ja värskendatakse P2 välimust
        ai.getShotInfoProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> {
                P2.updateBoard();
                setNextPlayer();

            });

        //Kolmas tähtis koht game-flow seisukohast on GraphicalUICell.rect.setOnMouseClicked, mis käsitleb mängija valikut.

        });



    }






    public static void setupGUI() {

        GridPane grid = new GridPane();

        //seoks cell'i suuruse vaid gridi laiusega?
        grid.setPadding(new Insets(10, 10, 10, 10)); //(top/right/bottom/left)
        grid.setVgap(30);


        //suuruse muutmisega tegelemine:
        DoubleBinding BoardArea = grid.widthProperty().divide(2).subtract(90);
        System.out.println(P1);
        System.out.println(P2);

        P1.resizeToFit(BoardArea);
        P2.resizeToFit(BoardArea);


        //See kast, kuhu AI oma kommentaare puistama hakkab.
        TextArea chatbox = new TextArea();
        chatbox.setPrefHeight(150);
        chatbox.positionCaret(chatbox.getLength());
        chatbox.setId("chatBox");


        grid.add(P1, 0, 0);
        grid.add(P2, 1, 0);
        grid.add(chatbox, 0, 2);
        grid.setColumnSpan(chatbox, 2);

        //prooviks midagi auto-resizema saada. Esmalt grid.
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(50);
        grid.getColumnConstraints().add(column1);

        //chatbox võib olla nii suur kui vähegi saab.
        chatbox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        Scene scene = new Scene(grid, 900, 680);
        primaryStage.setTitle("The Ultimate Battleship v2.0");
        primaryStage.setMinWidth(450);
        primaryStage.setMinHeight(400);
        primaryStage.setScene(scene);
        primaryStage.show();

        //ESC avab menüü.
        scene.setOnKeyPressed(e -> {
                if (e.getCode() == KeyCode.ESCAPE) {
                    Menu.mainMenu();
                }
            });



        //Vajutades ülevalt "X" avatakse main menu
        primaryStage.setOnCloseRequest(e -> {
        	e.consume();
        	Menu.mainMenu();
        });  
    }

    //updateBoard teeb muuhulgas kontrolli, kas plats on tühi. Kui on, siis kutsub selle siin välja.
    public static void gameOver(boolean playerWon) {
        System.out.println("Game Over. ");
        menuMessage = (playerWon) ? "You won..:(" : "Ha, loser!";
        Menu.mainMenu();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args); //see on blocking.
    }

}