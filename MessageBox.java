import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class MessageBox {

	
    public static void messageWindow(String message) {
        Stage window = new Stage();
        
        //loon stage-i
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Teade!");
        window.setWidth(280); 
        window.setHeight(110);

        //loon ok nupu
        Label label = new Label();
        label.setText(message);
        Button okButton = new Button("OK");
        okButton.setOnAction(e -> {
        	window.close();
        });
        okButton.setMinWidth(50);
        

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, okButton);
        layout.setAlignment(Pos.BASELINE_CENTER);
        
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.setResizable(false);
        window.showAndWait();
    }
	
}
