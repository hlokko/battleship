import java.io.*;

import javafx.animation.FadeTransition;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.util.Duration;


//Tõenäoliselt tegeleb see klass salvestamise ja laadimisega (mida see tegelikult teeb on näha siis kui testida jõuab)
public class Saving{
	
	

	//Nii saveGame kui ka loadGame tagastavad booleani edukuse kohta
	public static boolean saveGame(String failiNimi) {

        try{
			ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(failiNimi + ".lahing"));

            output.writeBoolean(GraphicalUI.isPlayersTurn());
            output.writeObject(GraphicalUI.getAi().getOwnBoard()); //sest ainult AI teab, kus ta laevad on..
			output.writeObject(GraphicalUI.getP2().board);

            System.out.println(GraphicalUI.getP1());
            System.out.println(GraphicalUI.getP2());

        }
		catch(IOException e){
             //mis siin toimub?
			 final StringWriter sw = new StringWriter();
		     final PrintWriter pw = new PrintWriter(sw, true);
		     e.printStackTrace(pw);
		     System.out.println(sw.getBuffer().toString());
             return false;
        }
        return true;
	}



	public static boolean loadGame(String failiNimi){//TODO teha load korda

		try{
			ObjectInputStream input = new ObjectInputStream(new FileInputStream(failiNimi + ".lahing"));

            GraphicalUI.setPlayersTurn( input.readBoolean() );
            //GraphicalUI.setP1( new GraphicalUIBoard( (BattleBoard) input.readObject(), false ) ); //see false on kasOnMängijaVäli
            //GraphicalUI.setP2( new GraphicalUIBoard( (BattleBoard) input.readObject(), true  ) );

            BattleBoard b1 = (BattleBoard) input.readObject();
            BattleBoard b2 = (BattleBoard) input.readObject();
            input.close();


            GraphicalUI.getP1().board.setBoard( b1.getBoard() ); //TODO: oeh, see laadimise asi on nüüd selline nõme häkk, et on kohe. Aga mis sa teed, kui ei oska alguses arhitektuuri paika panna.
            GraphicalUI.getP2().board.setBoard( b2.getBoard() );
            GraphicalUI.getAi().setOpponentsBoard(b2);
            GraphicalUI.getAi().setOwnBoard(b1);

            GraphicalUI.getP1().updateBoard();
            GraphicalUI.getP2().updateBoard();


            //P1 P2 peavad juba olemas olema, et see häkk töötaks
        }
		catch(IOException | ClassNotFoundException e){
			return false;
		}
		return true;
	}


	//Kuna salvestatakse ja laetakse sama arvu failidesse, siis piisab ühest meetodist mõlema akna jaoks
	public static Button saveButton(boolean savingOP, int num){
		Button saveB;
        if (savingOP) {
            saveB = new Button("Salvestus " + num);
        } else {
            saveB = new Button("Laadimine " + num);
            File f = new File("save" + num + ".lahing");
            if(!f.exists()) {  //Kui faili ei ole, siis disabled
                saveB.setDisable(true);
                saveB.setStyle("-fx-background-color: #c0c0c0;");
            }
        }

        saveB.setOnAction(e -> {
        	if(savingOP){
	        	if(saveGame("save" + num)){
	        		MessageBox.messageWindow("salvestamine õnnestus");//TODO avada messagebox
	        	}else{
	        		MessageBox.messageWindow("Salvestamine nurjus!");//TODO sulgeda ka salvestamise scene
	        	}
        	}
	        else{
	        	if(loadGame("save" + num)){
	        		System.out.println("Laadimine õnnestus!");//TODO avada messagebox
	        		//GraphicalUI.getP1().updateBoard(); //ülal juba on, mis küll millegipärast mängu sees ei mõika?
	        		//GraphicalUI.getP2().updateBoard();
	        		GraphicalUI.setMenuMessage("Battleship");
	        	}else{
	        		MessageBox.messageWindow("Laadimine nurjus!");
	        	}
	        }
            //kuidas siit saaks menüü kinni panna? ja miks ta seda mõnikord ei tee?
            //System.out.println("peaks menüü sulgema");
            //TODO: minor bug: kui salvestatud on lõppseisumäng (gameOver), siis mäng laeb, tuleb kohe gameover menüü ette, ja sealt võetud loadgame enam eest ära ei lähe. AltF4 aitab.
            //
            Menu.closeMenu();
            //tegelikult oleks hea midagi, mis vahetaks menüü tasandit - et salvestamise puhul hüppaks peamenüüsse, laadimise puhul mängu.

        });//TODO muuta prindid meetodite väljakutsumise vastu
        
        saveB.setMinWidth(Menu.getNupuLaius());//fade ini korraldamine läheb natuke keerulisemaks kui ma arvasin
        saveB.setMinHeight(Menu.getNupuKorgus());
        saveB.setOpacity(0.7);
        return saveB;
	}
	
	
	
}
