
//ikkagi sisuliselt coords... Aga leiab tarvitust hoopis sõnumivahetuses
public class CellInfo {

    int x;
    int y;
    BattleBoard.Cell type;

    public CellInfo(){};
    public CellInfo(int x, int y, BattleBoard.Cell type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public boolean isHit () {return this.type == BattleBoard.Cell.HIT;}




    @Override
    public String toString() {
        return "CellInfo{" +
                "x=" + x +
                ", y=" + y +
                ", type=" + type +
                '}';
    }

}
